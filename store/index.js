export const state = () => ({
  portfolio: [],
  assets: [],
  debt: [],
  count: 0,
  currency: "EUR",
})


export const getters = {
  getPortfolio (state) {
    return state.portfolio;
  },
  getAssets (state) {
    return state.assets;
  },
  getDebt (state) {
    return state.debt;
  },
  currency(state) {
    return state.currency
  }
}

export const actions = {
  add(context, arr){
      context.commit("ADD", arr);
  },
  remove(context, args){
    context.commit("REMOVE", args);
  },
  update(context, args){
    context.commit("UPDATE", args);
  }
}

export const mutations = {
  UPDATE(state, {arr, id, field, value}){
    const index = state[arr].findIndex(item => item._id === id);
    state[arr][index][field] = value;
  },
  ADD(state, arr, _id) {
    state[arr].push({ _id: ++state.count, name: "", amount: null });
  },
  REMOVE(state, {arr, id}) {
    const newArr = state[arr].filter(item => item._id !== id);
    state[arr] = newArr;
  }
}

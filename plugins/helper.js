export const formatter = (state) => {

  const country = state.currency === "EUR" ? "pt-PT" : "en-US";

  return new Intl.NumberFormat(country, {
    style: "currency",
    currency: state.currency
  });
}
